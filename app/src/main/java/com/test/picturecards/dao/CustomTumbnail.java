package com.test.picturecards.dao;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import java.io.File;
import java.util.List;

import it.gmariotti.cardslib.library.internal.CardThumbnail;

/**
 * Created by pohilko on 01.12.2014.
 */
public class CustomTumbnail extends CardThumbnail {

    private String url;

    public CustomTumbnail(Context context, String url) {
        super(context);
        this.url = url;
        //for using storage
        setExternalUsage(true);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, final View viewImage) {
        //set display and caching configs for ImageLoader
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE_SAFE)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        //set main configs for ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext())
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(2 * 1024 * 1024)
                .writeDebugLogs()
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        //loading image with caching in external storage to thumbnail view
        imageLoader.displayImage(url, (ImageView) viewImage, options, new SimpleImageLoadingListener() {
            boolean cacheFound;

            @Override
            public void onLoadingStarted(String url, View view) {
                List<String> memCache = MemoryCacheUtils.findCacheKeysForImageUri(url, ImageLoader.getInstance().getMemoryCache());
                cacheFound = !memCache.isEmpty();
                if (!cacheFound) {
                    File discCache = DiskCacheUtils.findInCache(url, ImageLoader.getInstance().getDiskCache());
                    if (discCache != null) {
                        cacheFound = discCache.exists();
                    }
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (cacheFound) {
                    MemoryCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getMemoryCache());
                    DiskCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getDiskCache());

                    ImageLoader.getInstance().displayImage(imageUri, (ImageView) view);
                }

                //set size of thumbnailview from loaded image
                viewImage.getLayoutParams().width = loadedImage.getWidth();
                viewImage.getLayoutParams().height = loadedImage.getHeight();
            }
        });
    }
}
