package com.test.picturecards.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.test.picturecards.R;
import com.test.picturecards.dao.CustomTumbnail;
import com.test.picturecards.utils.PreferencesUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.extra.staggeredgrid.view.CardGridStaggeredView;
import it.gmariotti.cardslib.library.extra.staggeredgrid.internal.CardGridStaggeredArrayAdapter;

public class CardsActivity extends ActionBarActivity {

    //for getting views using ButterKnife library
    @InjectView(R.id.cgvCards) CardGridStaggeredView gridView;
    @InjectView(R.id.btnReset) Button btnReset;

    private static final String BASE_KITTEN_URL = "https://placekitten.com/g/%d/%d";

    private Animation fadeIn;
    private Animation fadeOut;

    private ArrayList<Card> cards = new ArrayList<Card>();
    private Random rnd = new Random();
    private PreferencesUtil preferencesUtil;
    private CardGridStaggeredArrayAdapter cardArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);
        ButterKnife.inject(this);

        loadAnimations();
        preferencesUtil = PreferencesUtil.getInstance(this);
        loadCards();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        ImageLoader.getInstance().destroy();
    }

    @OnClick(R.id.btnReset)
    public void resetClickListener(View view){
        //clear card list and preferences
        cards.clear();
        preferencesUtil.clear();
        cardArrayAdapter.clear();
        loadCards();
        setupViews();
    }

    private void loadCardsFromHttp(){
        Set<String> urls = new HashSet<String>();
        for (int i = 0; i < 50; i++) {
            int width = rnd.nextInt(924) + 100;
            int height = rnd.nextInt(924) + 100;

            String url = String.format(BASE_KITTEN_URL, width, height);
            urls.add(url);
            Log.d("PictureKitten", "url=" + url);
            CustomTumbnail cardThumbnail = new CustomTumbnail(this, url);

            Card card = new Card(this);
            card.addCardThumbnail(cardThumbnail);
            cards.add(card);
        }
        preferencesUtil.saveUrls(urls);
    }

    private void loadCards(){
        if( preferencesUtil.getImagesSizes().size() == 0 ) {
            loadCardsFromHttp();
        } else {
            Set<String> urls = preferencesUtil.getUrls();
            Iterator<String> iterator = urls.iterator();
            while (iterator.hasNext() ){
                String url = iterator.next();

                Log.d("PictureKitten", "url=" + url);
                CustomTumbnail cardThumbnail = new CustomTumbnail(this, url);

                Card card = new Card(this);
                card.addCardThumbnail(cardThumbnail);
                cards.add(card);
            }
        }

        setupViews();
    }

    private void loadAnimations(){
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
    }

    //configure gridcardview and set cards
    private void setupViews(){
        cardArrayAdapter = new CardGridStaggeredArrayAdapter(this, cards);
        gridView.setAdapter(cardArrayAdapter);
        gridView.setOnScrollListener(scrollListener);
    }

    //hide btnReset for animation completing
    private void hideButton(){
        if( btnReset.getVisibility() != View.INVISIBLE ) {
            btnReset.startAnimation(fadeIn);
            btnReset.setVisibility(View.INVISIBLE);
        }
    }

    //show btnReset for animation completing
    private void showButton(){
        if( btnReset.getVisibility() != View.VISIBLE ) {
            btnReset.startAnimation(fadeOut);
            btnReset.setVisibility(View.VISIBLE);
        }
    }

    //custom listener for scroll detects and animate button
    private AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {

        private int oldTop;
        private int oldFirstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView absListView, int scrollState) {}

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            View view = absListView.getChildAt(0);
            int top = (view == null) ? 0 : view.getTop();

            //evaluate scroll direction
            if (firstVisibleItem == oldFirstVisibleItem) {
                if (top > oldTop) {
                    //SCROLLING UP
                    showButton();
                } else if (top < oldTop) {
                    //SCROLLING DOWN
                    hideButton();
                }
            } else {
                if (firstVisibleItem < oldFirstVisibleItem) {
                    //SCROLLING UP
                    showButton();
                } else {
                    //SCROLLING DOWN
                    hideButton();
                }
            }

            oldTop = top;
            oldFirstVisibleItem = firstVisibleItem;
        }
    };
}
