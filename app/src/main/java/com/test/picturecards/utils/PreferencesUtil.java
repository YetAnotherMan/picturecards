package com.test.picturecards.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by pohilko on 01.12.2014.
 */
public class PreferencesUtil {

    private Context context;

    private static PreferencesUtil instance;
    private static final String PREF_NAME = "img_sizes";
    private static final String IMAGE_SIZES_KEY = "sizes";
    private static final String IMAGE_URLS_KEY = "urls";

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private PreferencesUtil(Context context){
        prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public static PreferencesUtil getInstance(Context context){
        if( instance == null )
            instance = new PreferencesUtil(context);

        return instance;
    }

    public void saveUrls(Set<String> urls){
        editor.putStringSet(IMAGE_URLS_KEY, urls);
        editor.commit();
    }

    public Set<String> getUrls(){
        return prefs.getStringSet(IMAGE_URLS_KEY, new HashSet<String>());
    }

    public void saveImageSize(int width, int heigth){
        int key = 0;
        while( prefs.contains(IMAGE_SIZES_KEY+key) ){
            key++;
        }

        editor.putInt(IMAGE_SIZES_KEY + key, width);
        editor.putInt(IMAGE_SIZES_KEY + key, heigth);
        editor.commit();
    }

    public ArrayList<Pair<Integer, Integer>> getImagesSizes(){
        ArrayList<Pair<Integer, Integer>> sizes = new ArrayList<Pair<Integer, Integer>>();
        int key = 0;
        while( prefs.contains(IMAGE_SIZES_KEY+key) ){
            int width = prefs.getInt(IMAGE_SIZES_KEY + key, 0);
            int heigth = prefs.getInt(IMAGE_SIZES_KEY+key, 0);
            Pair<Integer, Integer> pair = new Pair<Integer, Integer>(width, heigth);
            sizes.add(pair);
            key++;
        }

        return sizes;
    }

    public void clear(){
        editor.clear();
        editor.commit();
    }
}
